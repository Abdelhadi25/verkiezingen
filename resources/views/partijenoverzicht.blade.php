@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <a href="{{route('partijtoevoegen')}}">
                    <button type="button" class="btn btn-secondary">Partij toevoegen</button>
                </a>
            </div>
            <div class="col-md-2">
                <a href="{{route('kamerlidtoevoegen')}}">
                    <button type="button" class="btn btn-secondary">Kamerlid toevoegen</button>
                </a>
            </div>
        </div>

            <table class="table" style="margin-top: 60px">
                <thead>
                    <tr>
                        <th scope="col">Partij</th>
                        <th scope="col">Kamerleden</th>
                        <th scope="col">Actie</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($partijen as $partij)
                    <tr>
                        <td>{{$partij['name']}}</td>
                        <td>
                            @foreach($partij['kamer_leden'] as $kamer_lid_name)
                                ({{$kamer_lid_name['kamer_lid_name']}})
                            @endforeach
                        </td>
                        <td><a href="php/edit.php?edit=<?php //echo "$id";?>"class="btn btn-outline-secondary">Edit</a><a href="php/delete.php?delete=<?php //echo "$id";?>"class="btn btn-outline-danger">Delete</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
         </div>
    </div>

@endsection
