@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Partij toevoegen') }}</div>

                    <div class="card-body">

                        <form method="POST" action="{{ route('partijtoevoegen') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="partij_naam" class="col-md-4 col-form-label text-md-right">{{ __('Partij naam') }}</label>

                                <div class="col-md-6">
                                    <input id="partij_naam" type="text" class="form-control @error('partij_naam') is-invalid @enderror" name="partij_naam" value="{{ old('partij_naam') }}" required autocomplete="partij_naam" autofocus >

                                    @error('partij_naam')
                                    <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Opslaan') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>@endsection
