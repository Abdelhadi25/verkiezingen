@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Kamerlid toevoegen') }}</div>

                    <div class="card-body">

                        <form method="POST" action="{{ route('kamerlidtoevoegen') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Partij naam') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus >

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="partijen" class="col-md-4 col-form-label text-md-right">{{ __('Partijen:') }}</label>
                                <div class="col-md-6">
                                    <br>
                                @foreach($partijen as $partij)
                                        <div class="pb-9">
                                            <input type="radio" id="partij_id" name="partij_id" value="{{$partij->id}}">
                                            <label for="partij_naam"> {{ $partij->name}}</label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Opslaan') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>@endsection
