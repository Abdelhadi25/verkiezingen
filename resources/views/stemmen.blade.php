@extends('layouts.app')
@section('content')
    <div class="container">

        <form method="POST" action="{{ route('stemmen') }}">

            @csrf

            <input type="hidden" name="bsn" value="{{$BSN}}">
        <div class="row justify-content-center">

            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">{{ __('Stem 1') }}</div>
                    <div class="card-body">
                        @foreach($partijen as $partij)
                            <div class="row">
                                <div class="pb-9">
                                    <input type="radio" id="partij" name="stem_1" style="margin-left: 30px" value="{{$partij->id}} " required>
                                    <label for="partij_naam"> {{ $partij->name}}</label>
                                </div>
                            </div>
                            @foreach($partij['kamer_leden'] as $kamer_lid)
                                <div class="row">
                                    <div class="pb-9">

                                        <input type="radio" id="kamer_lid" name="stem_1" style="margin-left: 60px" value="{{$kamer_lid->partij_id}} , {{ $kamer_lid->id}}">
                                        <label for="partij_naam"> {{ $kamer_lid['kamer_lid_name']}}</label>
                                    </div>
                                </div>
                            @endforeach
                        @endforeach
                    </div>
                </div>


            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">{{ __('Stem 2') }}</div>
                    <div class="card-body">
                        @foreach($partijen as $partij)
                            <div class="row">
                                <div class="pb-9">
                                    <input type="radio" id="partij" name="stem_2" style="margin-left: 30px" value="{{$partij->id}}"required>
                                    <label for="partij_naam"> {{ $partij->name}}</label>
                                </div>
                            </div>
                            @foreach($partij['kamer_leden'] as $kamer_lid)
                                <div class="row">
                                    <div class="pb-9">

                                        <input type="radio" id="kamer_lid" name="stem_2" style="margin-left: 60px" value="{{$kamer_lid->partij_id}} , {{ $kamer_lid->id}}">
                                        <label for="partij_naam"> {{ $kamer_lid['kamer_lid_name']}}</label>
                                    </div>
                                </div>
                            @endforeach
                        @endforeach
                    </div>
                </div>


            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">{{ __('Stem 3') }}</div>
                    <div class="card-body">
                        @foreach($partijen as $partij)
                            <div class="row">
                                <div class="pb-9">
                                    <input type="radio" id="partij" name="stem_3" style="margin-left: 30px" value="{{$partij->id}}" required>
                                    <label for="partij_naam"> {{ $partij->name}}</label>
                                </div>
                            </div>
                            @foreach($partij['kamer_leden'] as $kamer_lid)
                                <div class="row">
                                    <div class="pb-9">

                                        <input type="radio" id="kamer_lid" name="stem_3" style="margin-left: 60px" value="{{$kamer_lid->partij_id}} , {{ $kamer_lid->id}}">
                                        <label for="partij_naam"> {{ $kamer_lid['kamer_lid_name']}}</label>
                                    </div>
                                </div>
                            @endforeach
                        @endforeach
                    </div>
                </div>


            </div>

            <div class="form-group row" style="margin-top: 30px">
                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Opslaan') }}
                    </button>
                </div>
            </div>


        </div>
        </form>
    </div>
    </div>
@endsection
