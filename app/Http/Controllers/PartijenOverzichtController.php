<?php

namespace App\Http\Controllers;

use App\Models\Kamer_leden;
use App\Models\Partijen;
use Illuminate\Http\Request;

class PartijenOverzichtController extends Controller
{
    public function index() {

        $partijen = Partijen::all();

        foreach ($partijen as $partij) {
            $partij['kamer_leden'] = Kamer_leden::where('partij_id', '=', $partij['id'])->get();
        }
        return view('partijenoverzicht', compact('partijen'));
    }

    public function  getpagepartijtoevoegen() {
        return view('partijtoevoegen');
    }
    public function  getpagekamerlidtoevoegen() {
        $partijen = Partijen::all();

        return view('kamerlidtoevoegen' , compact('partijen'));
    }

    public function partijtoevoegen(Request $request) {
        $partij = new Partijen();
        $partij['name'] = $request->partij_naam;
        $partij->save();


        return view('partijenoverzicht');
    }
    public function kamerlidtoevoegen(Request $request) {
        $kamer_leden = new Kamer_leden();
        $kamer_leden['name'] = $request->name;
        $kamer_leden['partij_id'] = $request->partij_id;
        $kamer_leden->save();


        return view('partijenoverzicht');
//        return redirect()->back()->with('message', 'partij is verwijderd');
//        @if (Session::has('message'))
//        <div class="alert alert-success">{{ Session::get('message') }}</div>
//        @endif
    }
}
