<?php

namespace App\Http\Controllers;

use App\Models\Kamer_leden;
use App\Models\Partijen;
use App\Models\Stemmen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ResultaatOverzichtController extends Controller
{
    public function index() {
        $partijen = Partijen::all();
        $kamer_leden = Kamer_leden::all();

        foreach ($partijen as $partij) {
            $resultPartijen = Stemmen::select('eerste_p')->where('eerste_p', '=', $partij['id']);
            $count = $resultPartijen->count();
        }
        return $count;
        return view('resultaatoverzicht');
    }


}
