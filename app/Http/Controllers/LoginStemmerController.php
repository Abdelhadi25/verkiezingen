<?php

namespace App\Http\Controllers;

use App\Models\Stemmen;
use App\Models\Kamer_leden;
use App\Models\Partijen;
use Illuminate\Http\Request;
use function GuzzleHttp\Promise\all;

class LoginStemmerController extends Controller
{
    public function index() {
        return view('loginstemmer');
    }

    public function savebsn(Request $request) {
        $allbsn = Stemmen::select('bsn')->where('BSN' , '=', $request->BSN)->first();

        $partijen = Partijen::all();

        foreach ($partijen as $partij) {
            $partij['kamer_leden'] = Kamer_leden::where('partij_id', '=', $partij['id'])->get();
        }
//#1 klaar
        if ($allbsn === null){
            $bsn = new Stemmen();
            $bsn['BSN'] = $request->BSN;
            $bsn->save();
        }
            $BSN = $request->BSN;
            return view('stemmen', compact('BSN', 'partijen'));


    }

    public function stemmen(Request $request) {
//        return $request['stem_2'][4];
        $query = Stemmen::where('BSN', '=', $request['bsn']);
            if (!isset($request['stem_1'][4])) {
                $query

                ->update([
                'eerste_p' => $request['stem_1'][0],
                'eerste_k' => null,
                'tweede_p' => $request['stem_2'][0],
                'tweede_k' => $request['stem_2'][4],
                'deerde_p' => $request['stem_3'][0],
                'deerde_k' => $request['stem_3'][4],
                ]);
            }
        if (!isset($request['stem_2'][4])) {
            $query

                ->update([
                    'eerste_p' => $request['stem_1'][0],
                    'eerste_k' => $request['stem_1'][4],
                    'tweede_p' => $request['stem_2'][0],
                    'tweede_k' => null,
                    'deerde_p' => $request['stem_3'][0],
                    'deerde_k' => $request['stem_3'][4],

                ]);
        }
        if (!isset($request['stem_3'][4])) {
            $query

                ->update([
                    'eerste_p' => $request['stem_1'][0],
                    'eerste_k' => $request['stem_1'][4],
                    'tweede_p' => $request['stem_2'][0],
                    'tweede_k' => $request['stem_2'][4],
                    'deerde_p' => $request['stem_3'][0],
                    'deerde_k' => null,

                ]);
        }
        if (!isset($request['stem_1'][4]) && !isset($request['stem_2'][4])) {
            $query

                ->update([
                    'eerste_p' => $request['stem_1'][0],
                    'eerste_k' => null,
                    'tweede_p' => $request['stem_2'][0],
                    'tweede_k' => null,
                    'deerde_p' => $request['stem_3'][0],
                    'deerde_k' => $request['stem_3'][4],

                ]);
        }
        if (!isset($request['stem_2'][4]) && !isset($request['stem_3'][4])) {
            $query

                ->update([
                    'eerste_p' => $request['stem_1'][0],
                    'eerste_k' => $request['stem_1'][4],
                    'tweede_p' => $request['stem_2'][0],
                    'tweede_k' => null,
                    'deerde_p' => $request['stem_3'][0],
                    'deerde_k' => null,

                ]);
        }
        if (!isset($request['stem_1'][1]) && !isset($request['stem_3'][4])) {
            $query

                ->update([
                    'eerste_p' => $request['stem_1'][0],
                    'eerste_k' => null,
                    'tweede_p' => $request['stem_2'][0],
                    'tweede_k' => $request['stem_2'][4],
                    'deerde_p' => $request['stem_3'][0],
                    'deerde_k' => null,

                ]);
        }if (!isset($request['stem_1'][1]) && !isset($request['stem_2'][4]) && !isset($request['stem_3'][4])) {
            $query

                ->update([
                    'eerste_p' => $request['stem_1'][0],
                    'eerste_k' => null,
                    'tweede_p' => $request['stem_2'][0],
                    'tweede_k' => null,
                    'deerde_p' => $request['stem_3'][0],
                    'deerde_k' => null,

                ]);
        } elseif (isset($request['stem_1'][1]) && isset($request['stem_2'][4]) && isset($request['stem_3'][4])){
            $query
                ->update([
                    'eerste_p' => $request['stem_1'][0],
                    'eerste_k' => $request['stem_1'][4],
                    'tweede_p' => $request['stem_2'][0],
                    'tweede_k' => $request['stem_2'][4],
                    'deerde_p' => $request['stem_3'][0],
                    'deerde_k' => $request['stem_3'][4],

                ]);
        }

        return view('jouw_stemmen' );
    }
}
