<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kamer_leden extends Model
{
    use HasFactory;

    protected $table = 'kamer_leden';

    protected $fillable = [
        'id',
        'kamer_lid_name',
        'partij_id'
    ];
    public $timestamps = false;

}
