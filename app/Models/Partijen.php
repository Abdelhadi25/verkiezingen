<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Partijen extends Model
{
    use HasFactory;

    protected $table = 'partijen';

    protected $fillable = [
        'id',
        'name'
    ];
    public $timestamps = false;

}
