<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stemmen extends Model
{
    use HasFactory;

    protected $table = 'stemmen';

    protected $fillable = [
        'id',
        'BSN',
        'eerste_p',
        'eerste_k',
        'tweede_p',
        'tweede_k',
        'deerde_p',
        'deerde_k',
    ];
    public $timestamps = false;

}
