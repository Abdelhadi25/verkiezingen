<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKamerLedenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamer_leden', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kamer_lid_name', '50');
            $table->integer('partij_id')->unsigned();
            $table->foreign('partij_id')->references('id')->on('partijen');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamer_leden');
    }
}
