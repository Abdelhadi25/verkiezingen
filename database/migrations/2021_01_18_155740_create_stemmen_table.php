<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStemmenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stemmen', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('BSN');
            $table->integer('eerste_p')->unsigned()->nullable();
            $table->foreign('eerste_p')->references('id')->on('partijen');
            $table->integer('eerste_k')->unsigned()->nullable();
            $table->foreign('eerste_k')->references('id')->on('kamer_leden');
            $table->integer('tweede_p')->unsigned()->nullable();
            $table->foreign('tweede_p')->references('id')->on('partijen');
            $table->integer('tweede_k')->unsigned()->nullable();
            $table->foreign('tweede_k')->references('id')->on('kamer_leden');
            $table->integer('deerde_p')->unsigned()->nullable();
            $table->foreign('deerde_p')->references('id')->on('partijen');
            $table->integer('deerde_k')->unsigned()->nullable();
            $table->foreign('deerde_k')->references('id')->on('kamer_leden');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stemmen');
    }
}
