<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('loginstemmer');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/loginstemmer', [App\Http\Controllers\LoginStemmerController::class, 'index'])->name('loginstemmer');
Route::post('/loginstemmerstemmen', [App\Http\Controllers\LoginStemmerController::class, 'savebsn'])->name('loginstemmerstemmen');
Route::get('/resultaatoverzicht', [App\Http\Controllers\ResultaatOverzichtController::class, 'index'])->name('resultaatoverzicht');
Route::get('/partijenoverzicht', [App\Http\Controllers\PartijenOverzichtController::class, 'index'])->name('partijenoverzicht');
Route::get('/partijtoevoegen', [App\Http\Controllers\PartijenOverzichtController::class, 'getpagepartijtoevoegen'])->name('partijtoevoegen');
Route::get('/kamerlidtoevoegen', [App\Http\Controllers\PartijenOverzichtController::class, 'getpagekamerlidtoevoegen'])->name('kamerlidtoevoegen');
Route::post('/partijtoevoegen', [App\Http\Controllers\PartijenOverzichtController::class, 'partijtoevoegen'])->name('partijtoevoegen');
Route::post('/kamerlidtoevoegen', [App\Http\Controllers\PartijenOverzichtController::class, 'kamerlidtoevoegen'])->name('kamerlidtoevoegen');
Route::post('/stemmen', [App\Http\Controllers\LoginStemmerController::class, 'stemmen'])->name('stemmen');

